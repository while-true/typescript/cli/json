#!/usr/bin/env node
import { Command } from 'commander';
import { JsonModule } from './main';
import * as upath from 'upath';
// import * as packageJson from '../package.json';

const program = new Command();
program
  .name('json')
  .version('0.1.0');

program
  .command('synchronize <source> <destination...>')
  .description('Synchronize from a source json or js file to a destinations glob patterns.')
  .requiredOption('-p, --property <props...>', 'Lodash compatible property or properties paths to copy.')
  .option('-c, --cwd <cwd>', 'Sets the current working directory')
  .action((source: string, destinations: string[], props) => {
    const json = new JsonModule({ cwd: props.cwd || upath.resolve(process.cwd()) });
    return json.synchronize(source, destinations, props.property);
  });

program.parse();
