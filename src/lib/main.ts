import * as lodash from 'lodash';
import * as glob from 'glob';
import * as jsonfile from 'jsonfile';
import * as upath from 'upath';

/**
 * Represents a resolved target json file.
 */
export interface Target {
  /**
   * The path of the target file.
   */
  path: string;

  /**
   * The resolved json object.
   */
  json: object | [];
}

/**
 * Represents the global options of JsonModule.
 */
export interface GlobalJsonOptions {
  /**
   * The current working directory
   */
  cwd: string;
}

/**
 * Represents the options available to the command 'synchronize'.
 */
export interface SynchOptions extends GlobalJsonOptions {
  /**
   * When true, the source string is treated as a json string, otherwhise is treated as a file path.
   * Default: false;
   */
  inlineSource: boolean;
}

/**
 * This is the json cli module!
 */
export class JsonModule {

  /**
   * Receives the options for the new instance.
   * @param _options global configuration.
   */
  constructor(private readonly _options: GlobalJsonOptions) {

  }

  /**
   * Resolves the source object from the source path.
   * @param _src a path to a json file.
   */
  public resolveSource(_src: string, _cwd = this._options.cwd, _inline: boolean = false): object | [] {
    if (_inline) {
      const object = JSON.parse(_src);
      return object;
    }
    if (/.*.js$/.test(_src)) {
      return require(upath.resolve(_cwd, _src));
    }
    return jsonfile.readFileSync(upath.resolve(_cwd, _src));
  }

  /**
   * Resolves the source object from the source path.
   * @param _src a path to a json file.
   */
  public resolveTargets(_tgt: string | string[], _cwd = this._options.cwd): Promise<Promise<Target>[]> {
    const globs = lodash.castArray(_tgt);
    const globPromises: Promise<void>[] = [];
    const targetPromises: Promise<Target>[] = [];
    let doneTargets: (_tgts: Promise<Target>[]) => void;
    const targetsReadyPromise: Promise<Promise<Target>[]> =
      new Promise((_done, _error) => { doneTargets = _done; });

    globs.forEach((_pattern: string) => {
      let doneGlob: () => void;
      let errorGlob: (_err: any) => void;
      globPromises.push(new Promise<void>((_done, _error) => { [doneGlob, errorGlob] = [_done, _error]; }));
      glob(_pattern, { root: upath.resolve(_cwd), cwd: upath.resolve(_cwd), absolute: true }, (_error, _paths) => {
        if (_error) {
          // tslint:disable-next-line: no-console
          console.error(_error);
          errorGlob(_error);
          return;
        }
        _paths.forEach((_path) => {
          let done: (_res: Target) => void;
          let error: (_err: any) => void;
          targetPromises.push(new Promise<Target>((_done, _error) => { [done, error] = [_done, _error]; }));
          jsonfile.readFile(_path)
            .then((_json: any) => {
              done({
                path: _path,
                json: _json,
              });
            })
            .catch((_error: any) => {
              // tslint:disable-next-line: no-console
              console.error(_error);
              error(_error);
            });
        });
        doneGlob();
      });
    });

    let doneGlobsCount = 0;
    globPromises.forEach((promise) => {
      promise.finally(() => {
        doneGlobsCount += 1;
        if (doneGlobsCount === globPromises.length) {
          doneTargets(targetPromises);
        }
      });
    });

    return targetsReadyPromise;
  }

  /**
   * This command synchronize two json files at specific paths.
   */
  synchronize(_src: string, _tgt: string | string[], _paths: [string], _options?: SynchOptions): Promise<void> {
    const source: object = this.resolveSource(_src, _options?.cwd, _options?.inlineSource);
    let targetWritesDone: () => void;
    let targetWritesCount = 0;
    const synchronizePromise = new Promise<void>((_done, _error) => { targetWritesDone = _done; });
    this.resolveTargets(_tgt)
      .then((_targetPromises: Promise<Target>[]) => {
        _targetPromises.forEach((_targetPromise: Promise<Target>) => {
          _targetPromise
            .then((_target: Target) => {
              _paths.forEach((_jsonPath) => {
                lodash.set(_target.json, _jsonPath, lodash.get(source, _jsonPath));
                jsonfile.writeFile(_target.path, _target.json, { encoding: 'utf8', spaces: 2 })
                  .catch((_error) => {
                    // tslint:disable-next-line: no-console
                    console.error(_error);
                  })
                  .finally(() => {
                    targetWritesCount += 1;
                    if (targetWritesCount === _targetPromises.length) {
                      targetWritesDone();
                    }
                  });
              });
            });
        });
      });
    return synchronizePromise;
  }
}
