import { assert } from 'chai';
import 'mocha';
import { JsonModule } from '../main';
import * as jsonfile from 'jsonfile';
import * as mkdirp from 'mkdirp';
import * as upath from 'upath';

describe('Tests for JsonModule', () => {
  it(
    'Copy a single field',
    (done) => {
      mkdirp.sync(upath.resolve(__dirname, './.tmp/single'));
      const source = { property1: 'value1', property2: 'value2' };
      jsonfile.writeFileSync(upath.resolve(__dirname, './.tmp/single/source.json'), source);
      jsonfile.writeFileSync(upath.resolve(__dirname, './.tmp/single/target.json'), {});
      const json = new JsonModule({ cwd: __dirname });
      json.synchronize('./.tmp/single/source.json', './.tmp/single/target.json', ['property1'])
        .then(() => {
          const newTarget = jsonfile.readFileSync(upath.resolve(__dirname, './.tmp/single/target.json'));
          assert.hasAllKeys(newTarget, ['property1'], 'Target must not have more keys than "property1"');
          const newSource = jsonfile.readFileSync(upath.resolve(__dirname, './.tmp/single/source.json'));
          assert.deepEqual(source, newSource, 'Source must never be changed.');
          done();
        });
    });
  it(
    'Copy a deep property',
    (done) => {
      mkdirp.sync(upath.resolve(__dirname, './.tmp/inner'));
      const deep = { deep: 'deep1' };
      const source = { property1: deep, property2: 'value2' };
      jsonfile.writeFileSync(upath.resolve(__dirname, './.tmp/inner/source.json'), source);
      jsonfile.writeFileSync(upath.resolve(__dirname, './.tmp/inner/target.json'), {});
      const json = new JsonModule({ cwd: __dirname });
      json.synchronize('./.tmp/inner/source.json', './.tmp/inner/target.json', ['property1.deep'])
        .then(() => {
          const newTarget = jsonfile.readFileSync(upath.resolve(__dirname, './.tmp/inner/target.json'));
          assert.hasAllKeys(newTarget, ['property1'], 'Target must not have more keys than "property1"');
          assert.deepEqual(newTarget, { property1: deep }, 'Target deep property was not correctly copied');
          const newSource = jsonfile.readFileSync(upath.resolve(__dirname, './.tmp/inner/source.json'));
          assert.deepEqual(source, newSource, 'Source must never be changed.');
          done();
        });
    });
  it(
    'Copy to a non empty target',
    (done) => {
      mkdirp.sync(upath.resolve(__dirname, './.tmp/non-empty'));
      const deep = { deep1: 'deep1' };
      const source = { property1: deep, property2: 'value2' };
      jsonfile.writeFileSync(upath.resolve(__dirname, './.tmp/non-empty/source.json'), source);
      jsonfile.writeFileSync(
        upath.resolve(__dirname, './.tmp/non-empty/target.json'),
        { property1: { deep2: 'deep2' }, property3: 'value3' });
      const json = new JsonModule({ cwd: __dirname });
      json.synchronize('./.tmp/non-empty/source.json', './.tmp/non-empty/target.json', ['property1.deep1'])
        .then(() => {
          const newTarget = jsonfile.readFileSync(upath.resolve(__dirname, './.tmp/non-empty/target.json'));
          assert.deepEqual(newTarget, { property1: { deep1: 'deep1', deep2: 'deep2' }, property3: 'value3' }, 'Target deep properties was not correctly copied');
          const newSource = jsonfile.readFileSync(upath.resolve(__dirname, './.tmp/non-empty/source.json'));
          assert.deepEqual(source, newSource, 'Source must never be changed.');
          done();
        });
    });

});
